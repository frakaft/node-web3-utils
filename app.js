const express = require('express')
const app = express()
app.use(express.json())
const port = 3000

const Web3 = require('web3')
const web3 = new Web3("ws://localhost:7545")

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/block-number', async (req, res) => {

  console.log(res)

  var blockNumber = await web3.eth.getBlockNumber()
  var response = {
    "number": blockNumber
  }
  res.send(response)
})

app.get('/accounts', async (req, res) => {

  console.log(res)

  var accounts = await web3.eth.getAccounts()
  var response = {
    "accounts": accounts
  }
  res.send(response)
})

app.get('/balance/:wallet', async (req, res) => {

  console.log(res)

  var account = req.params.wallet;
  var balance = await web3.eth.getBalance(account);
  var unit = req.query.unit;
  var convertedBalance = web3.utils.fromWei(balance, unit) 
  
  var response = {
    "account": account,
    "balance": convertedBalance,
    "unit": unit
  }
  res.send(response)
})

app.post('/transfer', async (req, res) => {

  console.log(res)

  var body = req.body;
  var response = await web3.eth.sendTransaction(body);
  
  res.send(response)
})


app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
